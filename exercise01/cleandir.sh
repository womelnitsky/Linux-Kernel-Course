#!/bin/bash

CMD_ARGS=("$@")
SPECIFIED_DIR=$(pwd)
HELP_FLAG=false
RECURSIVE_FLAG=false
TEST_FLAG=false
IS_DIR_WAS_SPECIFIED=false
DIRS_TO_CHECK=()
FILES_TO_DELETE=()

reverse_array() {
    # first argument is the array to reverse
    # second is the output array
    declare -n arr="$1" rev="$2"
    for i in "${arr[@]}"; do
        rev=("$i" "${rev[@]}")
    done
}

for (( i=0;i<$#;i++)); do
    if [ ${CMD_ARGS[$i]} = "--help" ] || [ ${CMD_ARGS[$i]} = "-h" ]; then
        HELP_FLAG=true
    elif [ ${CMD_ARGS[$i]} = "--test" ] || [ ${CMD_ARGS[$i]} = "-t" ]; then
        TEST_FLAG=true
    elif [ ${CMD_ARGS[$i]} = "--recursive" ] || [ ${CMD_ARGS[$i]} = "-r" ]; then
        RECURSIVE_FLAG=true
    elif [ "$IS_DIR_WAS_SPECIFIED" = false ]; then 
        SPECIFIED_DIR=${CMD_ARGS[$i]}
        IS_DIR_WAS_SPECIFIED=true
    else 
        echo "Wrong options"
        exit 1
    fi
done 

if  $HELP_FLAG; then
    echo "Usage: ./cleandir.sh [OPTION]... [DIR]..."
    echo "Delete files (*.tmp, _*, ~*, -*) in specified directory (the current directory by default)."

    echo "Mandatory arguments to long options are mandatory for short options too."
    echo "  -h, --help              print help info ."
    echo "  -t, --test              print paths to files to be deleted, but do not delete files themselves"
    echo "  -r, --recursive         perform deletion in all subdirectories and delete empty directories 
                          after recursive deletion (excluding the initial one)."
    exit 0
fi

if [ -d $SPECIFIED_DIR ]; then 
    echo "Search dir was setted: $SPECIFIED_DIR"
else
    echo "No such directory"
    exit 2
fi

if $RECURSIVE_FLAG; then
    for dir in $(find $SPECIFIED_DIR -type d)
    do
        DIRS_TO_CHECK+=($dir)
    done
else
    DIRS_TO_CHECK+=($SPECIFIED_DIR)
fi

for dir in "${DIRS_TO_CHECK[@]}"; do
    for file in $(find $dir -maxdepth 1 -mindepth 1 -type f \( -name "~*" -o -name "_*" -o -name "-*" -o -name "*.tmp" \)); do
        FILES_TO_DELETE+=($file)
    done
done

for file in "${FILES_TO_DELETE[@]}"; do
    if $TEST_FLAG; then
        echo "$file"
    else
        rm $file
    fi
done

if [ "$RECURSIVE_FLAG" = true ] && [ "$TEST_FLAG" = false ]; then
    echo $'\nDeleted directories:'
    reverse_array DIRS_TO_CHECK DIRS_TO_DELETE
    for dir in "${DIRS_TO_DELETE[@]}"; do
        if [ $(find $dir -mindepth 1 -maxdepth 1 | wc -l) -eq 0 ] && [ $dir != $SPECIFIED_DIR ]; then
            rm -rf $dir
            echo "$dir"
        fi
    done
fi

